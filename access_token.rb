require "sinatra"
require "httparty"
require "base64"

class CI
  include HTTParty
  base_uri 'https://idbroker.webex.com'
end

set :client_id, ""
set :client_secret, ""
set :redirect_uri, ""
set :webex_sitename, ""
set :email, ""

def auth_xml(email)
<<~XML
<?xml version='1.0' encoding='UTF-8'?>
<serv:message xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xmlns:com=\"http://www.webex.com/schemas/2002/06/common\"
xmlns:use=\"http://www.webex.com/schemas/2002/06/service/user\"  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">
  <serv:header>
    <serv:securityContext>
      <com:webExID>#{email}</com:webExID>
      <com:siteName>#{settings.webex_sitename}</com:siteName>
    </serv:securityContext>
  </serv:header>
  <serv:body>
    <serv:bodyContent xsi:type=\"java:com.webex.xmlapi.service.binding.user.AuthenticateUser\">
      <use:accessToken>#{@access_token}</use:accessToken>
    </serv:bodyContent>
  </serv:body>
</serv:message>
XML
end

get "/start_flow" do
  response_type = "code"
  scope = CGI.escape("cloudMeetings:login")
  state = "start_flow"
  @link = "#{CI.base_uri}/idb/oauth2/v1/authorize?response_type=#{response_type}&client_id=#{settings.client_id}&redirect_uri=#{CGI.escape(settings.redirect_uri)}&scope=#{scope}&state=#{state}"
  erb :index
end

get "/token" do
  grant_type = "authorization_code"
  code = params[:code]
  auth_for_basic = Base64.strict_encode64("#{settings.client_id}:#{settings.client_secret}")
  body = { "grant_type" => grant_type, "code" => code, "redirect_uri" => settings.redirect_uri }
  headers = { "Content-Type" => "application/x-www-form-urlencoded", "Authorization" => "Basic #{auth_for_basic}" }
  options = { :body => body, :headers => headers }
  res = CI.post("/idb/oauth2/v1/access_token",options)
  @access_token = res["access_token"]
  @refresh_token = res["refresh_token"]
  @xml = auth_xml(settings.email)
  erb :token
end
