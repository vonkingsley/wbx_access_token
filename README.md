# Prereq

Go to [webexdeveloper](http://webexdeveloper.com)

Login or create an account

1. Goto "My Account" => "My apps"
2. Select "Add New App"
3. Fill out the app - 
4. Select cloudMeetings : cloudMeetings Login for the scope
(cloudMeetings:login)

5. Click create

6. Copy down your application key and application secret

## Install

```ruby
bundle install
```

## Setup access_token.rb

Fill out your client_id, client_secret, and redirect_uri for oAuth.

Fill out webex_sitename, and an email for XML API

## Run

```
ruby access_token.rb -p3000 -o0.0.0.0
```

Load localhost:3000/start_flow

## AuthenticateUser

The returned access code should show up in the example xml, just submit
the form on the page.
